package com.retail.core;



public class AirShippingCostCal implements ShippingCost {
	

	@Override
	public double shippingMethod(Item I ) {
		
		double res=0;
		
		long  t= I.getUpc();
		 t=  (t/10);
		res= t%10;
		double cost = I.getWeight()*res;
		cost=Math.round(cost * 100.0) / 100.0; 
		return cost;
		

	}


}
