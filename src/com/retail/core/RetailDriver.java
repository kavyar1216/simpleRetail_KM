package com.retail.core;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeMap;


public class RetailDriver {
	
	public static <T> void main(String args [])
	{
		
		TreeMap<Long,Item> treeMap = new TreeMap<>();
		
		
		Item I1= new Item(567321101987L,"CD � Pink Floyd, Dark Side Of The Moon",19.99,0.58,"AIR");
		Item I2= new Item(567321101986L,"CD � Beatles, Abbey Road              ",17.99,0.61,"GROUND");
		Item I3= new Item(567321101985L,"CD � Queen, A Night at the Opera      ",20.49,0.55,"AIR");
		Item I4= new Item(567321101984L,"CD � Michael Jackson, Thriller        ",23.88,0.50,"GROUND");
		Item I5= new Item(467321101899L,"iPhone - Waterproof Case	           ",9.75,0.73,"AIR");
		Item I6= new Item(477321101878L,"iPhone -  Headphones	               ",17.25,3.21,"GROUND");
		
		treeMap.put(I1.getUpc(),I1);
		treeMap.put(I2.getUpc(),I2);
		treeMap.put(I3.getUpc(),I3);
		treeMap.put(I4.getUpc(),I4);
		treeMap.put(I5.getUpc(),I5);
		treeMap.put(I6.getUpc(),I6);
		

		
		System.out.println(I1.test());
		System.out.println(I2.test());
		System.out.println(I3.test());
		System.out.println(I4.test());
		System.out.println(I5.test());
		System.out.println(I6.test());
		
		
		
		ShippingCostFactory shippingCostFactory = new ShippingCostFactory();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss"); 
		Date date= new Date();
		System.out.print("****SHIPMENT REPORT****"  );
		System.out.println( "         "+ dateFormat.format(date)  );
		System.out.println("     UPC      " + "         Description                             "+"  Price  "+"         Weight   "+ "    ShipMethod " + "  Shippingcost");
		double totalCost=0;

		for(Item items: treeMap.values()) {
			double cost= shippingCostFactory.shippingInfo(items);
			 totalCost+=cost;
	System.out.println(items.getUpc() +"      "+ items.getDesc()+"            "+items.getPrice()+"         "+items.getWeight()+"        "+items.getShippingMethod()+"          "+cost);
	
	
	
		}
		System.out.println();
		System.out.println("Total Shipping cost:"    +"                                                                                        "+ totalCost);

	

	}

	}

